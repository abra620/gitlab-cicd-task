
FROM openjdk:7
WORKDIR /usr/src/myapp
COPY HelloWorld.java /usr/src/myapp
RUN javac HelloWorld.java
CMD ["java", "HelloWorld"]

